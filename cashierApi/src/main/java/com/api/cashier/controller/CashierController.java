package com.api.cashier.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.api.cashier.dto.request.RetreatRequestDto;
import com.api.cashier.dto.request.StatsRequestDto;
import com.api.cashier.dto.response.CashierRetreatDto;
import com.api.cashier.entity.OperationEntity;
import com.api.cashier.service.CashierService;

import lombok.extern.log4j.Log4j2;

@RestController
@Log4j2
@RequestMapping("/")
public class CashierController {
	
	@Autowired
	private CashierService cashierService;
	
	@PostMapping(value = "/retire",consumes = "application/json", produces="application/json")
	public ResponseEntity<Object> retire(@RequestBody RetreatRequestDto request ) {
		CashierRetreatDto response = cashierService.retireAmount(request);
		if(response.getAmount() == null) {
			return new ResponseEntity<>(response,HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<>(response,HttpStatus.OK);
	}
	
	@PostMapping(value = "/balance",consumes = "application/json", produces="application/json")
	public ResponseEntity<Object> retire(@RequestBody StatsRequestDto request ) {
		CashierRetreatDto response = cashierService.getBalance(request);
		if(response.getAmount() == null) {
			return new ResponseEntity<>(response,HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<>(response,HttpStatus.OK);
	}
	
	
	@PostMapping(value = "/stats",consumes = "application/json", produces="application/json")
	public ResponseEntity<List<OperationEntity>> getStats(@RequestBody StatsRequestDto requestStats){
		log.info("Cliente: " + requestStats.getIdClient());
		List<OperationEntity> response = cashierService.searchOperationsByClient(requestStats);
		if(response == null || response.isEmpty()) {
			log.info("Actualmente no hay compras por parte de: " + requestStats.getIdClient());
			response = new ArrayList<>();
			return new ResponseEntity<>(response,HttpStatus.FORBIDDEN);
		}
		return new ResponseEntity<>(response,HttpStatus.OK);
	}
    
}
