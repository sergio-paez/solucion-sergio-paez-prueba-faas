package com.api.cashier.entity;

import java.io.Serializable;

import javax.persistence.*;
import lombok.Data;

@Entity
@Data
@Table(name="Account",schema="public")
public class AcountEntity implements Serializable {
	
	private static final long serialVersionUID = -1141337078867996859L;

	@Column
	@Id
	private String idAcount;
	
	@Column
	private Long numero;
	
	@Column
	private Integer pass;
	
	@Column
	private Long saldo;
	
	@Column
	private String idClient;
}
