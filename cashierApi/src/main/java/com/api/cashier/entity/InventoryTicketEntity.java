package com.api.cashier.entity;

import lombok.Data;

import java.io.Serializable;

import javax.persistence.*;

@Data
@Entity
@Table
public class InventoryTicketEntity implements Serializable {
	
	private static final long serialVersionUID = -6495584512042804126L;

	@Id
	@Column
	private String idTicket;
	
	@Column
	private String name;
	
	@Column
	private Integer amount;
	
	@Column
	private String value;

}
