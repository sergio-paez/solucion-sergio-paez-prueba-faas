package com.api.cashier.entity;

import java.io.Serializable;

import javax.persistence.*;
import lombok.Data;

@Entity
@Data
@Table(name="Client",schema="public")
public class ClientEntity implements Serializable{

	private static final long serialVersionUID = 5424485570406241226L;

	@Id
	@Column
	private String idClient;
	
	@Column
	private String name;
	
}
