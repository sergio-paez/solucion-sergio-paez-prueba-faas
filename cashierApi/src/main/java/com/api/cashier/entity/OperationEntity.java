package com.api.cashier.entity;

import javax.persistence.*;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name="Operation",schema="public")
@Data
public class OperationEntity implements Serializable {
	

	private static final long serialVersionUID = -8936138690240359967L;

	@Column
	@Id
	private String idOperation;
	
	@Column
	private String nameOperation;
	
	@Column
	private Date dateOperation;
	
	@Column
	private String idClient;
	
}
