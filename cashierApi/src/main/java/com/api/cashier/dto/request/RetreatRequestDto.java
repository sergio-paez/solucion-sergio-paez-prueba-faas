package com.api.cashier.dto.request;

import lombok.Data;

@Data
public class RetreatRequestDto {
	
	private String idClient;
	
	private Long valueTransaction;

}
