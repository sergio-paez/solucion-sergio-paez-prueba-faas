package com.api.cashier.dto.response;

import com.google.gson.annotations.SerializedName;
import lombok.Data;

@Data
public class CashierRetreatDto {
	
	@SerializedName("Amount")
	private Long amount;

}
