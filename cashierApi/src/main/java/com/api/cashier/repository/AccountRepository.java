package com.api.cashier.repository;

import org.springframework.data.repository.CrudRepository;

import com.api.cashier.entity.AcountEntity;

public interface AccountRepository extends CrudRepository<AcountEntity, String> {
	
	public AcountEntity findByidClient(String idClient);

}
