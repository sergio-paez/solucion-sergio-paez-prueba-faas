package com.api.cashier.repository;

import org.springframework.data.repository.CrudRepository;
import com.api.cashier.entity.OperationEntity;

import java.util.List;

public interface OperationRepository extends CrudRepository<OperationEntity, String> {
	
	List<OperationEntity> findByidClient(String idClient);

}
