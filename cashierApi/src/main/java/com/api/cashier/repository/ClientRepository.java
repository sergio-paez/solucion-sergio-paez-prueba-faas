package com.api.cashier.repository;

import com.api.cashier.entity.ClientEntity;
import org.springframework.data.repository.CrudRepository;

public interface ClientRepository extends CrudRepository<ClientEntity,String> {

}
