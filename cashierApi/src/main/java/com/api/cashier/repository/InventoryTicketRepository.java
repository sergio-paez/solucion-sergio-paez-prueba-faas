package com.api.cashier.repository;

import org.springframework.data.repository.CrudRepository;
import com.api.cashier.entity.InventoryTicketEntity;

public interface InventoryTicketRepository extends CrudRepository<InventoryTicketEntity, String>{

}
