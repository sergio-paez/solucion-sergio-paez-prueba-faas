package com.api.cashier.service;

import java.util.List;
import com.api.cashier.entity.*;
import com.api.cashier.dto.request.RetreatRequestDto;
import com.api.cashier.dto.request.StatsRequestDto;
import com.api.cashier.dto.response.CashierRetreatDto;

public interface CashierService {
	
	public CashierRetreatDto retireAmount(RetreatRequestDto request);
	
	public CashierRetreatDto getBalance(StatsRequestDto request);
	
	public List<OperationEntity> searchOperationsByClient(StatsRequestDto request);

}
