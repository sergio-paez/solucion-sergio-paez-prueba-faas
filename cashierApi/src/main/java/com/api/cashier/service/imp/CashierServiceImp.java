package com.api.cashier.service.imp;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.api.cashier.dto.request.RetreatRequestDto;
import com.api.cashier.dto.request.StatsRequestDto;
import com.api.cashier.entity.AcountEntity;
import com.api.cashier.entity.OperationEntity;
import com.api.cashier.repository.AccountRepository;
import com.api.cashier.repository.OperationRepository;
import com.api.cashier.service.CashierService;
import com.api.cashier.dto.response.CashierRetreatDto;
import lombok.extern.log4j.Log4j2;
import java.util.List;

@Service
@Log4j2
public class CashierServiceImp implements CashierService {
	
	@Autowired
	private AccountRepository acountRepository;
	
	@Autowired
	private OperationRepository operationRepository;

	@Override
	public CashierRetreatDto retireAmount(RetreatRequestDto request) {
		log.info("Iniciando servicio de retiro del cliente: " + request.getIdClient());
		CashierRetreatDto response = new CashierRetreatDto();
		AcountEntity clientAcountData = acountRepository.findByidClient(request.getIdClient());
		clientAcountData.setSaldo(clientAcountData.getSaldo() - request.getValueTransaction());
		acountRepository.save(clientAcountData);
		log.info("Finalizando servicio de retiro del cliente: " + request.getIdClient());
		
		OperationEntity operationEntity = new OperationEntity();
		//TODO: Registrar la operacion
		
		response.setAmount(clientAcountData.getSaldo());
		return response;
	}

	@Override
	public List<OperationEntity> searchOperationsByClient(StatsRequestDto request) {
		log.info("Iniciando Servicio de consulta de Operaciones del cliente: " + request.getIdClient());
		List<OperationEntity> response = operationRepository.findByidClient(request.getIdClient());
		log.info("Finalizando Servicio de consulta de Operaciones del cliente: " + request.getIdClient());
		return response;
	}

	@Override
	public CashierRetreatDto getBalance(StatsRequestDto request) {
		log.info("Iniciando servicio de consulta del cliente: " + request.getIdClient());
		CashierRetreatDto response = new CashierRetreatDto();
		AcountEntity clientAcountData = acountRepository.findByidClient(request.getIdClient());
		log.info("Finalizando servicio de consulta del cliente: " + request.getIdClient());
		response.setAmount(clientAcountData.getSaldo());
		return response;
	}

}
