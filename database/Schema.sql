CREATE TABLE Client(
	idClient varchar(50) NOT NULL,
	name VARCHAR(30) NOT NULL
);

alter table client add CONSTRAINT PK_Cliente PRIMARY KEY (idClient);


CREATE TABLE Account(
	idAcount VARCHAR(50) NOT NULL,
	numero BIGINT NOT NULL,
	pass INT NOT NULL,
	saldo BIGINT NOT NULL,
	idClient varchar(50) NOT null
);

alter table Account add CONSTRAINT PK_Cuenta PRIMARY KEY (idAcount);

CREATE TABLE Operation(
	idOperation VARCHAR(50) NOT NULL,
	nameOperation  VARCHAR(50) NOT NULL,
	dateOperation TIMESTAMP not null,
	idClient VARCHAR(50) not null,
	CONSTRAINT PK_Operacion PRIMARY KEY (idOperation)
);

INSERT INTO public.client
(idclient, "name")
VALUES('asdsa1234232sad', 'Sergio Paez');

INSERT INTO public.account
(idacount, numero, pass, saldo, idclient)
VALUES('asdsadsadas', 123456, 4151658, 10000, 'asdsa1234232sad');

INSERT INTO public.operation
(idoperation, nameoperation, dateoperation, idclient)
VALUES('1', 'Retiro', now(), 'asdsa1234232sad');

